$(document).ready(function(){
    $('.hero-container').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: false,
            });

      $('.slide-lançamento').slick({
        autoplaySpeed: 5000,

        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        autoplaySpeed: 3000,
        responsive: [
              {
                
                breakpoint: 512,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  arrows: false,
               
                }}

            ]



      });

      //responsive slick
      
    $(window).on('load resize orientationchange', function() {
      $('.slide-container').each(function(){
          var $carousel = $(this);
       
          if ($(window).width() > 512) {
              if ($carousel.hasClass('slick-initialized')) {
                  $carousel.slick('unslick');
              }
          }
          else{
              if (!$carousel.hasClass('slick-initialized')) {
                  $carousel.slick({
                      slidesToShow: 1,
                      autoplaySpeed: 5000,
                      slidesToScroll: 1,
                      mobileFirst: true,
                      arrows: false,

                  });
              }
          }
      });
  });
});``
    
 

  